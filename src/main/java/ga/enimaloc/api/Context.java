/*
 *  Copyright 2018 Antoine Sainty (antoinesainty23102003@gmail.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package ga.enimaloc.api;

import com.sun.net.httpserver.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class Context implements HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        ResponseHeader responseHeader = getFormattedResponse();
        try {
            if (httpExchange.getRequestMethod().equalsIgnoreCase(Method.GET)) responseHeader = GET(new ContextElements(httpExchange));
            else {
//                if (!httpExchange.getRequestHeaders().containsKey("Authorization")) responseHeader = getFormattedResponse(401, "Unauthorized", "authorization_header_required");
//                else if (Shlouk.INSTANCE.getTokenManager().getToken(httpExchange.getRequestHeaders().get("Authorization").get(0)) == null)
//                    responseHeader = getFormattedResponse(401, "Unauthorized", "incorrect_authorization_header");
                if (httpExchange.getRequestMethod().equalsIgnoreCase(Method.POST)) responseHeader = POST(new ContextElements(httpExchange));
                else if (httpExchange.getRequestMethod().equalsIgnoreCase(Method.PUT)) responseHeader = PUT(new ContextElements(httpExchange));
                else if (httpExchange.getRequestMethod().equalsIgnoreCase(Method.REMOVE)) responseHeader = REMOVE(new ContextElements(httpExchange));
                else if (httpExchange.getRequestMethod().equalsIgnoreCase(Method.PATCH)) responseHeader = PATCH(new ContextElements(httpExchange));
            }
        } catch (Exception e) {
            responseHeader = getFormattedResponse(500, "Internal Server Error", e.getLocalizedMessage());
//            Main.client.sendException(e);
            e.printStackTrace();
        }
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Headers", "origin, x-requested-with, content-type, Authorization");
        headers.add("Access-Control-Allow-Methods", "PUT, GET, POST, REMOVE, PATCH");
//        Header add Access-Control-Allow-Origin "*"
//        Header add Access-Control-Allow-Headers "origin, x-requested-with, content-type, Authorization"
//        Header add Access-Control-Allow-Methods "PUT, GET, POST, REMOVE, OPTIONS"
        httpExchange.sendResponseHeaders(responseHeader.getCode(), responseHeader.getMessage().toString().getBytes(Charset.forName("UTF-8")).length);
        OutputStream body = httpExchange.getResponseBody();
        body.write(responseHeader.getMessage().toString().getBytes(Charset.forName("UTF-8")));
        body.close();
    }

    public ResponseHeader GET(ContextElements contextElements) throws Exception {
        return getFormattedResponse();
    }
    public ResponseHeader POST(ContextElements contextElements) throws Exception {
        return getFormattedResponse();
    }
    public ResponseHeader PUT(ContextElements contextElements) throws Exception {
        return getFormattedResponse();
    }
    public ResponseHeader REMOVE(ContextElements contextElements) throws Exception {
        return getFormattedResponse();
    }
    public ResponseHeader PATCH(ContextElements contextElements) throws Exception {
        return getFormattedResponse();
    }

    public ResponseHeader getFormattedResponse() {
        return getFormattedResponse(200, "OK");
    }

    public ResponseHeader getFormattedResponse(int code, String msg) {
        return getFormattedResponse(code, msg, null);
    }

    public ResponseHeader getFormattedResponse(int code, String msg, Object content) {
        return getFormattedResponse(code, msg, content, null);
    }

    public ResponseHeader getFormattedResponse(int code, String msg, Object content, String error) {
        JSONObject json = new JSONObject();
        json.put("code", code);
        json.put("message", msg);
        if (content != null) json.put("content", content);
        if (error != null && !error.isEmpty()) json.put("error", error);
        return new ResponseHeader(code, json);
    }

    public class ContextElements extends HttpExchange {

        private HttpExchange httpExchange;

        public ContextElements(HttpExchange httpExchange) {
            this.httpExchange = httpExchange;
        }

        @Override
        public Headers getRequestHeaders() {
            return httpExchange.getRequestHeaders();
        }

        @Override
        public Headers getResponseHeaders() {
            return httpExchange.getResponseHeaders();
        }

        @Override
        public URI getRequestURI() {
            return httpExchange.getRequestURI();
        }

        @Override
        public String getRequestMethod() {
            return httpExchange.getRequestMethod();
        }

        @Override
        public HttpContext getHttpContext() {
            return httpExchange.getHttpContext();
        }

        @Override
        public void close() {
            httpExchange.close();
        }

        @Override
        public InputStream getRequestBody() {
            return httpExchange.getRequestBody();
        }

        @Override
        public OutputStream getResponseBody() {
            return httpExchange.getResponseBody();
        }

        @Override
        public void sendResponseHeaders(int i, long l) throws IOException {
            httpExchange.sendResponseHeaders(i, l);
        }

        @Override
        public InetSocketAddress getRemoteAddress() {
            return httpExchange.getRemoteAddress();
        }

        @Override
        public int getResponseCode() {
            return httpExchange.getResponseCode();
        }

        @Override
        public InetSocketAddress getLocalAddress() {
            return httpExchange.getLocalAddress();
        }

        @Override
        public String getProtocol() {
            return httpExchange.getProtocol();
        }

        @Override
        public Object getAttribute(String s) {
            return httpExchange.getAttribute(s);
        }

        @Override
        public void setAttribute(String s, Object o) {
            httpExchange.setAttribute(s, o);
        }

        @Override
        public void setStreams(InputStream inputStream, OutputStream outputStream) {
            httpExchange.setStreams(inputStream, outputStream);
        }

        @Override
        public HttpPrincipal getPrincipal() {
            return httpExchange.getPrincipal();
        }

        public Map<String, String> getQuery() {
            if (getRequestURI().getQuery() == null ||
                getRequestURI().getQuery().isEmpty() ||
                getRequestURI().getQuery().equalsIgnoreCase(""))
                return new HashMap<>();
            Map<String, String> query = new HashMap<>();
            String[] query1 = getRequestURI().getQuery().split("&");
            for (String q : query1) query.put(q.split("=")[0], q.split("=")[1]);
            return query;
        }
    }

    public class ResponseHeader {

        private int code;
        private Object message;

        public ResponseHeader(int code, Object message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public Object getMessage() {
            return message;
        }
    }

    public class Method {
        public static final String GET = "GET";
        public static final String POST = "POST";
        public static final String PUT = "PUT";
        public static final String REMOVE = "REMOVE";
        public static final String PATCH = "PATCH";
    }
}
