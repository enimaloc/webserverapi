/*
 *  Copyright 2018 Antoine Sainty (antoinesainty23102003@gmail.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package ga.enimaloc.api;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class WebServer {

    private final Map<String, HttpHandler> contexts;
    private final int port;
    private final HttpServer server;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public WebServer(Map<String, HttpHandler> contexts, int port) throws IOException {
        this.contexts = contexts;
        this.port = port;
        this.server = HttpServer.create(new InetSocketAddress(this.port), 0);
        addContexts(contexts);
        server.setExecutor(null);
        log.info("WebServer started on port "+port);
        server.start();
    }

    public void addContexts(Map<String, HttpHandler> contexts) {
        contexts.forEach(this::addContext);
    }

    public void addContext(String name, HttpHandler handler) {
        log.info("Add `" + name + "` methods with " + handler.getClass().getSimpleName() + " class");
        server.createContext(name, handler);
        this.contexts.put(name, handler);
    }

    public void removeContexts(List<String> names) {
        names.forEach(this::removeContext);
    }

    public void removeContext(String name) {
        log.info("Remove `" + name + "` methods");
        server.removeContext(name);
        this.contexts.remove(name);
    }

    public void stop(int i) {
        log.info("WebServer stopped");
        server.stop(i);
    }

    public HttpServer getServer() {
        return server;
    }

    public Map<String, HttpHandler> getContexts() {
        return contexts;
    }

    public HttpHandler getContextClass(String name) {
        AtomicReference<HttpHandler> tr = null;
        getContexts().forEach((s, c) -> {
            if (s.equalsIgnoreCase(name)) tr.set(c);
        });
        return tr.get();
    }

    public String getContextName(Class<? extends HttpHandler> context) {
        AtomicReference<String> tr = new AtomicReference<>("");
        getContexts().forEach((s, c) -> {
            if (c.getClass().getSimpleName().equalsIgnoreCase(context.getSimpleName())) tr.set(s);
        });
        return tr.get();
    }
}
